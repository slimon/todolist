from django import forms
from .models import Task

class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        exclude = ['task_assigned_to']
        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Title'}),
            'description': forms.Textarea(attrs={
                'class': 'form-control',
                'rows': 5}),
            'status': forms.Select(attrs={
                'class': 'form-select',
                'placeholder': 'Status'
            }),
            'project': forms.Select(attrs={
                'class': 'form-select',
                'placeholder': 'Project name'
            })
            }
        
class TaskFormUpdate(forms.ModelForm):
    class Meta:
        model = Task
        exclude = ['task_assigned_to']
        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Title'}),
            'description': forms.Textarea(attrs={
                'class': 'form-control',
                'rows': 5}),
            'status': forms.Select(attrs={
                'class': 'form-select',
                'placeholder': 'Status'}),
            }

class SearchForm(forms.Form):
    search = forms.CharField(max_length=50, required=False, label='Search')

class TaskUserChangeForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ['task_assigned_to']
        widgets = {'task_assigned_to': forms.SelectMultiple(attrs={'class': 'form-select'})}

class TaskProjectChangeForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ['project']
        widgets = {'project': forms.Select(attrs={'class': 'form-select'})}