from typing import Any
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect
from .models import Task
from .forms import TaskForm, SearchForm, TaskFormUpdate, TaskUserChangeForm, TaskProjectChangeForm
from projects.forms import ProjectForm
from django.views.generic import DetailView, CreateView, ListView, UpdateView, DeleteView
from django.urls import reverse, reverse_lazy
from django.db.models import Q
from django.utils.http import urlencode
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin

class IndexView(ListView):
    template_name = 'index.html'
    model = Task
    context_object_name = 'tasks'

class TaskDetailView(DetailView):
    template_name = 'tasks/detail.html'
    model = Task

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        project_form = ProjectForm()
        my_task = self.object
        project = my_task.project
        context['project'] = project
        context['project_form'] = project_form
        return context
    
    
class TaskCreateView(LoginRequiredMixin, CreateView):
    template_name = 'tasks/create.html'
    model = Task
    form_class = TaskForm   
    
    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        self.object.task_assigned_to.add(self.request.user)
        return HttpResponseRedirect(self.get_success_url())
    
    def get_success_url(self):
        return reverse('task_detail', kwargs={'pk': self.object.pk})
    
class TaskListView(ListView):
    template_name = 'tasks/list.html'
    model = Task
    form = SearchForm
    context_object_name = 'tasks'
    ordering = ['pk']
    paginate_by = 5
    paginate_orphans = 1
        
    def get(self, request, *args, **kwargs):
        self.form = self.get_search_form()
        self.search_value = self.get_search_value()
        return super().get(request, *args, **kwargs)
    
    def get_search_form(self):
        return self.form(self.request.GET)
    
    def get_search_value(self):
        if self.form.is_valid():
            return self.form.cleaned_data.get('search')
        return None
    
    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        context['form'] = self.form
        if self.search_value:
            context['query'] = urlencode({'search': self.search_value})
        return context
    
    def get_queryset(self):
        queryset = super().get_queryset()
        sort_by = self.request.GET.get('sort_by')
        if self.search_value:
            query = Q(title__icontains=self.search_value)
            queryset = queryset.filter(query)        
        elif sort_by:
            queryset = queryset.order_by(sort_by)
        return queryset
       

class TaskUpdateView(PermissionRequiredMixin, UpdateView):
    model = Task
    template_name = 'tasks/update.html'
    form_class = TaskFormUpdate
    context_object_name = 'task'
    permission_required = ['to_do_list.change_task']

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        project_form = ProjectForm()
        my_task = self.object
        project = my_task.project
        context['project'] = project
        context['project_form'] = project_form
        return context

    def get_success_url(self) -> str:
        return reverse('task_detail', kwargs={'pk': self.object.pk})
    

class TaskDeleteView(PermissionRequiredMixin, DeleteView):
    template_name = 'tasks/delete.html'
    model = Task
    context_object_name = 'task'
    success_url = reverse_lazy('task_list')
    permission_required = ['to_do_list.delete_task']

class TaskUsersUpdateView(PermissionRequiredMixin, UpdateView):
    model = Task
    template_name = 'tasks/manage_users_task.html'
    form_class = TaskUserChangeForm
    context_object_name = 'task'
    permission_required = ['to_do_list.delete_task']

    def get_success_url(self) -> str:
        return reverse('task_detail', kwargs={'pk': self.object.pk})
    
class SelectProjectUpdateView(PermissionRequiredMixin, UpdateView):
    model = Task
    template_name = 'tasks/select_project.html'
    form_class = TaskProjectChangeForm
    context_object_name = 'task'
    permission_required = ['projects.add_project']

    def get_success_url(self) -> str:
        return reverse('task_detail', kwargs={'pk': self.object.pk})
    
