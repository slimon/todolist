from django.contrib import admin
from to_do_list.models import Task
from projects.models import Project

class TaskAdmin(admin.ModelAdmin):
    list_display = ['title', 'description', 'status']
    list_filter = ['title', 'task_assigned_to']
    fields = ['title', 'description', 'status', 'task_assigned_to', 'project']

admin.site.register(Task, TaskAdmin)

class ProjectAdmin(admin.ModelAdmin):
    list_display = ['name', 'description', 'created_at']
    fields = ['name', 'description', 'member']

admin.site.register(Project, ProjectAdmin)
