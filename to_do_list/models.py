from django.db import models
from django.contrib.auth import get_user_model
from projects.models import Project

class Task(models.Model):
    class Status(models.TextChoices):
        Inprogress = 'In progress'
        Done = 'Done'
        Pending = 'Pending'

    title = models.CharField(max_length=100, null=False, blank=False, verbose_name='Title')
    description = models.CharField(max_length=500, null=True, blank=True, verbose_name='Description')
    status = models.CharField(max_length = 50, choices=Status.choices, default=Status.Inprogress, verbose_name='Status')
    task_assigned_to = models.ManyToManyField(get_user_model(), related_name='tasks', blank=True)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Creation date and time')
    project = models.ForeignKey('projects.Project', on_delete=models.CASCADE, null=True, blank=True, related_name='tasks', verbose_name='Project')

    def __str__(self):
        return f'{self.title}'
    
    def assigned_to(self):
        return ", ".join([str(i) for i in self.task_assigned_to.all()])
    
    def project_assigned(self):
        return f'{self.project}'

    