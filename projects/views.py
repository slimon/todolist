from typing import Any
from django.http import HttpResponseRedirect
from accounts.admin import User
from .models import Project
from .forms import ProjectForm, ProjectUserChangeForm
from to_do_list.forms import TaskForm
from django.views.generic import DetailView, CreateView, ListView, UpdateView, DeleteView
from django.urls import reverse, reverse_lazy
from django.db.models import Q
from django.utils.http import urlencode
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin


class ProjectDetailView(DetailView):
    template_name = 'projects/detail.html'
    model = Project

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        task_form = TaskForm
        my_project = self.object
        task = my_project.tasks
        context['task'] = task
        context['task_form'] = task_form
        return context
    
    
class ProjectCreateView(PermissionRequiredMixin, CreateView):
    template_name = 'projects/create.html'
    model = Project
    form_class = ProjectForm 
    permission_required = ['projects.add_project']  

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        self.object.member.add(self.request.user)
        return HttpResponseRedirect(self.get_success_url())
    
    def get_success_url(self) -> str:
        return reverse('project_detail', kwargs={'pk': self.object.pk})
    
class ProjectListView(ListView):
    template_name = 'projects/list.html'
    model = Project
    form = ProjectForm
    context_object_name = 'projects'
    ordering = ['pk']
    paginate_by = 5
    paginate_orphans = 1
        
    def get(self, request, *args, **kwargs):
        self.form = self.get_search_form()
        self.search_value = self.get_search_value()
        return super().get(request, *args, **kwargs)
    
    def get_search_form(self):
        return self.form(self.request.GET)
    
    def get_search_value(self):
        if self.form.is_valid():
            return self.form.cleaned_data.get('search')
        return None
    
    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        context['form'] = self.form
        if self.search_value:
            context['query'] = urlencode({'search': self.search_value})
        return context
    
    def get_queryset(self):
        queryset = super().get_queryset()
        sort_by = self.request.GET.get('sort_by')
        if self.search_value:
            query = Q(name__icontains=self.search_value)
            queryset = queryset.filter(query)        
        elif sort_by:
            queryset = queryset.order_by(sort_by)
        return queryset
       

class ProjectUpdateView(PermissionRequiredMixin, UpdateView):
    model = Project
    template_name = 'projects/update.html'
    form_class = ProjectForm
    context_object_name = 'project'
    permission_required = ['projects.change_project']

    def get_success_url(self) -> str:
        return reverse('project_detail', kwargs={'pk': self.object.pk})
    

class ProjectDeleteView(PermissionRequiredMixin, DeleteView):
    template_name = 'projects/delete.html'
    model = Project
    context_object_name = 'project'
    success_url = reverse_lazy('project_list')
    permission_required = ['projects.delete_project']

class ProjectUsersUpdateView(PermissionRequiredMixin, UpdateView):
    model = Project
    template_name = 'projects/manage_users.html'
    form_class = ProjectUserChangeForm
    context_object_name = 'project'
    permission_required = ['to_do_list.delete_task']

    def get_success_url(self) -> str:
        return reverse('project_detail', kwargs={'pk': self.object.pk})