from django.db import models
from django.contrib.auth import get_user_model

class Project(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False, verbose_name='Name')
    description = models.CharField(max_length=500, null=True, blank=True, verbose_name='Description')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Creation Date')
    member = models.ManyToManyField(get_user_model(), related_name='projects', blank=True)

    def __str__(self):
        return self.name

    def members(self):
        return ", ".join([str(i) for i in self.member.all()])
