from django.urls import path
from .views import ProjectDetailView, ProjectCreateView, ProjectUpdateView, ProjectDeleteView, ProjectListView, ProjectUsersUpdateView



urlpatterns = [
    path('detail/<int:pk>', ProjectDetailView.as_view(), name='project_detail'),
    path('create/', ProjectCreateView.as_view(), name='project_create'),
    path('list/', ProjectListView.as_view(), name='project_list'),
    path('update/<int:pk>', ProjectUpdateView.as_view(), name='project_update'),
    path('delete/<int:pk>', ProjectDeleteView.as_view(), name='project_delete'),
    path('detail/<int:pk>/manage', ProjectUsersUpdateView.as_view(), name='manage_users')
]

