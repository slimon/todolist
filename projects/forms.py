from django import forms
from .models import Project


class ProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Project name'}),
            'description': forms.Textarea(attrs={
                'class': 'form-control',
                'rows': 5}),
            'member': forms.SelectMultiple(attrs={
                'class': 'form-select'
            })
            }

class ProjectUserChangeForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = ['member']
        widgets = {'member': forms.SelectMultiple(attrs={'class': 'form-select'})}