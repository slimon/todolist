from django.db import models
from django.contrib.auth import get_user_model

class Profile(models.Model):
    user = models.OneToOneField(get_user_model(), related_name='profile', on_delete=models.CASCADE, verbose_name='User')
    occupation = models.CharField(max_length = 100, null=True, blank=True, verbose_name='Occupation')
    tel = models.CharField(max_length=50, null=True, blank=True, verbose_name='Telephone')
    avatar = models.ImageField(null=True, blank=True, upload_to='user_pics', verbose_name='Avatar')

    def __str__(self):
        return self.user.get_full_name() + "'s Profile"

        