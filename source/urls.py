"""
URL configuration for source project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.conf import settings
from django.conf.urls.static import static
from to_do_list.views import (
    IndexView,
    TaskDetailView,
    TaskCreateView,
    TaskListView,
    TaskUpdateView,
    TaskDeleteView,
    TaskUsersUpdateView,
    SelectProjectUpdateView
)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', IndexView.as_view(), name='home_page'),
    path('detail/<int:pk>', TaskDetailView.as_view(), name='task_detail'),
    path('create/', TaskCreateView.as_view(), name='task_create'),
    path('list/', TaskListView.as_view(), name='task_list'),
    path('update/<int:pk>', TaskUpdateView.as_view(), name='task_update'),
    path('delete/<int:pk>', TaskDeleteView.as_view(), name='task_delete'),
    path('detail/<int:pk>/manage', TaskUsersUpdateView.as_view(), name='manage_users_task'),
    path('detail/<int:pk>/project', SelectProjectUpdateView.as_view(), name='select_project'),
    path('accounts/', include('accounts.urls')),
    path('projects/', include('projects.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
